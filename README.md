# Zwiki

Zwiki is a static wiki system based on reStructuredText files.

Rendering reStructuredText files is a very flexible approach to generating wiki pages, because of reStructuredText's expressiveness and extensibility. It's expressive ways to write references (see <http://docutils.sourceforge.net/docs/user/rst/quickref.html#hyperlink-targets>) surpass those of basic Markdown's (see <https://daringfireball.net/projects/markdown/syntax#link>) by far. In context of wiki systems especially document internal links and wiki internal links are of importance.

# Requirements

Depending on how you wish to run Zwiki, dependencies vary. If you want to run Zwiki as a Docker container, the dependency will be Docker. In an uncontainerized setup the requirements will include libraries Zwiki depends on.

## Libraries

Zwiki depends on the following libraries:

* `docutils` -- for translating reStructuredText to HTML
* `jinja2` -- for translating Jinja 2 templates to HTML pages
* `gunicorn` -- for running WSGI containers
* `falcon` -- for implementing the API
* `pygments` -- for rendering code blocks in Zwiki pages

# Running

## Uncontainerized

This project requires a few libraries to work. I recommend creating a fresh environment to run Zwiki:

```
$ conda create --name zwiki python=3.7 docutils jinja2 gunicorn pygments
$ source activate zwiki
$ conda install falcon -c conda-forge
```

With the environment activated, you can run Zwiki using the following command:

```
$ gunicorn -b 0.0.0.0:5000 main:api --log-config log.conf
```

(If you are developing running `gunicorn`, you might consider using `--reload` to have it automatically watch files of Zwiki and reload, so that no restarting is necessary to see changes.)

Then access Zwiki in your browser at <http://localhost:5000/>.

## Running as a Docker container

Simply build an image using the provided `Dockerfile` and run it:

```docker run -d <your-image-name>```

Then access Zwiki in your browser at <http://localhost:5000/>.

If you run Zwiki as a Docker container, be aware of either having to rebuild the Docker image or having to modifying the Zwiki content inside the Docker container, while it is running.

# Outlook

* One idea is to make Zwiki automatically update its content from an external source. This could be a Git repository, which can be accessed using some deployment key. Then there are several ways to go about updating content. The updates could be user triggered or perhaps periodic.
* An idea is also to allow more formats to be used as sources for Zwiki pages. Some people might not need reStructuredText's capabilities and for them Markdown might suffice. Zwiki could distinguish between formats by file ending or it could get some kind of configuration file, which tells it as what format to interpret a file.
* A full text search functionality or at least titles search functionality would be great.
