# from latest debian:stable on 2018-08-10 approximately at 15:00
# FROM debian@sha256:272b4877ca42d22addd178b8d546fb78546fee40e2dae6b9316d21f048d2ecec
FROM debian@sha256:52af198afd8c264f1035206ca66a5c48e602afb32dc912ebf9e9478134601ec4

MAINTAINER zelphirkaltstahl@gmail.com

ENV META_IMAGE_NAME="zwiki"
ENV META_COURSE_VERSION="0.0.1"

###################
# SYSTEM PACKAGES #
###################
USER root
RUN REPO=http://cdn-fastly.deb.debian.org \
 && echo "deb $REPO/debian jessie main\ndeb $REPO/debian-security jessie/updates main" > /etc/apt/sources.list \
 && apt-get update && apt-get --yes --quiet dist-upgrade \
 && apt-get install --yes --quiet --no-install-recommends \
    libc6-dev libc-dev \
    gcc g++ \
    dpkg-dev \
    make \
    openssl \
    wget \
    curl \
    bzip2 \
    ca-certificates \
    sudo \
    locales \
    build-essential \
    python-dev \
    libsm6 \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*

############
# ENV VARS #
############
ARG DEBIAN_FRONTEND="noninteractive"
ENV SHELL /bin/bash
# user and group
ENV NON_PRIVILEGED_USER="zwiki"
ARG NON_PRIVILEGED_USER_PASSWORD="some password here"
ENV NON_PRIVILEGED_USER_GROUP="zwiki"

#################
# ADDING A USER #
#################
# For more info please check useradd --help or man useradd.
ENV HOME="/home/${NON_PRIVILEGED_USER}"
RUN groupadd -r $NON_PRIVILEGED_USER_GROUP -g 1000 \
 && useradd \
    --uid 1000 \
    --system \
    --gid $NON_PRIVILEGED_USER_GROUP \
    --create-home \
    --home-dir "${HOME}" \
    --shell /bin/bash \
    --comment "non-privileged user" \
    $NON_PRIVILEGED_USER \
 && chmod 755 "${HOME}" \
 && echo "${NON_PRIVILEGED_USER}:${NON_PRIVILEGED_USER_PASSWORD}" | chpasswd
# setup curl
RUN echo "cacert=/etc/ssl/certs/ca-certificates.crt" > "${HOME}/.curlrc"  # do we really need this? (for wget?)

##############
# SET LOCALE #
##############
RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
    echo 'LANG="en_US.UTF-8"'>/etc/default/locale && \
    dpkg-reconfigure --frontend=noninteractive locales && \
    update-locale LANG=en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

#################
# INSTALL CONDA #
#################
USER $NON_PRIVILEGED_USER

ARG CONDA_DIR="${HOME}/anaconda"

ARG MINICONDA_VERSION="4.5.4"
ARG MINICONDA_SHA256SUM="80ecc86f8c2f131c5170e43df489514f80e3971dd105c075935470bbf2476dea"
ARG MINICONDA_DOWNLOAD_URL="https://repo.continuum.io/miniconda/Miniconda3-${MINICONDA_VERSION}-Linux-x86_64.sh"
ARG MINICONDA_DOWNLOAD_FILENAME="miniconda.sh"

ARG CONDA_DIR="${HOME}/miniconda"
# add conda to path
ENV PATH $CONDA_DIR/bin:$PATH

WORKDIR "${HOME}"
RUN mkdir --parents $CONDA_DIR\
 && wget --quiet "${MINICONDA_DOWNLOAD_URL}" --output-document="${MINICONDA_DOWNLOAD_FILENAME}"\
 && echo "${MINICONDA_SHA256SUM}" "${MINICONDA_DOWNLOAD_FILENAME}" | sha256sum -c -\
 && /bin/bash $MINICONDA_DOWNLOAD_FILENAME -f -b -p $CONDA_DIR \
 && rm $MINICONDA_DOWNLOAD_FILENAME \
 && conda update -n base conda \
 && $CONDA_DIR/bin/conda config --system --add channels conda-forge \
 && $CONDA_DIR/bin/conda config --system --set auto_update_conda false \
 && conda clean -tips --yes

##################
# CONDA PACKAGES #
##################
USER $NON_PRIVILEGED_USER
RUN conda install --yes --quiet -c conda-forge \
    gunicorn=19.7.1 \
    falcon=1.4.1
RUN conda clean -tips --yes

################
# PIP PACKAGES #
################
# (stuff which is not on conda)
USER $NON_PRIVILEGED_USER

###############
# COPY SERVER #
###############
USER root
COPY app/ "${HOME}/app"
COPY start_zwiki.sh "${HOME}/start_zwiki.sh"
RUN chown $NON_PRIVILEGED_USER:$NON_PRIVILEGED_USER_GROUP --recursive "${HOME}/app"

############
# FINALIZE #
############
# PORT
EXPOSE 5000

# INITIAL DIRECTORY
WORKDIR /home/$NON_PRIVILEGED_USER/work

# switch back to jovyan to avoid accidental container runs as root
USER $NON_PRIVILEGED_USER

# configure container startup
CMD ["/bin/sh", "/home/tester/start_zwiki.sh"]
